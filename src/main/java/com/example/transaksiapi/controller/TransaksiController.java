package com.example.transaksiapi.controller;

import com.example.transaksiapi.helper.CSVHelper;
import com.example.transaksiapi.model.response.StdResponse;
import com.example.transaksiapi.service.TransaksiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api_fe")
public class TransaksiController {
    private static Logger logger = LoggerFactory.getLogger(TransaksiController.class);

    @Autowired
    TransaksiService transaksiService;

    @PostMapping("/postdata")
    public ResponseEntity<StdResponse<String>> uploadCSVTransaksi(@RequestParam("file") MultipartFile file){
        logger.info("========== START uploadCSVTransaksi with file name:"+file.getOriginalFilename()+" ==========");
        if (CSVHelper.hasCSVFormat(file)) {
            String fileName = file.getOriginalFilename();
            try {
                transaksiService.save(file);
            }catch (Exception e){
                StdResponse<String> response = new StdResponse<String>(HttpStatus.EXPECTATION_FAILED.toString(), "Could not upload the file", fileName);
                return new ResponseEntity<StdResponse<String>>(response, HttpStatus.EXPECTATION_FAILED);
            }

            StdResponse<String> response = new StdResponse<String>(HttpStatus.OK.toString(), "", fileName);
            return new ResponseEntity<StdResponse<String>>(response, HttpStatus.OK);
        }
        StdResponse<String> response = new StdResponse<String>(HttpStatus.BAD_REQUEST.toString(), "Format file invalid", "");
        logger.info("========== END uploadCSVTransaksi ==========");
        return new ResponseEntity<StdResponse<String>>(response, HttpStatus.BAD_REQUEST);
    }
}
