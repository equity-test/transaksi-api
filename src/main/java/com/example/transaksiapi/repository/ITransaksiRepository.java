package com.example.transaksiapi.repository;

import com.example.transaksiapi.model.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITransaksiRepository extends JpaRepository<Transaksi,Integer> {
}
