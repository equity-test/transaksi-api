package com.example.transaksiapi.repository;

import com.example.transaksiapi.model.entity.Fee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFeeRepository extends JpaRepository<Fee,Integer> {
}
