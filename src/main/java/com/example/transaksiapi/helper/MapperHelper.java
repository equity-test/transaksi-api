package com.example.transaksiapi.helper;

import com.example.transaksiapi.helper.ConvertDateHelper;
import com.example.transaksiapi.model.entity.Fee;
import com.example.transaksiapi.model.entity.Transaksi;
import org.apache.commons.csv.CSVRecord;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MapperHelper {
    public static Transaksi toTransaksi(CSVRecord csvRecord){
        Transaksi transaksi = new Transaksi();
        transaksi.setEmployeeId(Integer.parseInt(csvRecord.get("Employee_id")));
        transaksi.setAmount(BigDecimal.valueOf(Long.parseLong(csvRecord.get("Amount"))));
        transaksi.setTrxDate(ConvertDateHelper.convertStrDateHHMMSS(csvRecord.get("Tgl_transaksi")));
        transaksi.setCreateBy(1);
        transaksi.setCreateDate(LocalDateTime.now());
        transaksi.setUpdateBy(1);
        transaksi.setUpdateDate(LocalDateTime.now());

        return transaksi;
    }

    public static Fee toFee(Transaksi trx, BigDecimal feeAmount){
        Fee fee = new Fee();
        fee.setEmployeeId(trx.getEmployeeId());
        fee.setTrxId(trx.getTrxId());
        fee.setAmountFee(feeAmount);
        fee.setFeeDate(LocalDateTime.now());
        fee.setCreateBy(1);
        fee.setCreateDate(LocalDateTime.now());
        fee.setUpdateBy(1);
        fee.setUpdateDate(LocalDateTime.now());

        return fee;
    }
}
