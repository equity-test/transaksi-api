package com.example.transaksiapi.helper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ConvertDateHelper {
    public static LocalDateTime convertStrDateHHMMSS(String strDateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(strDateTime, formatter);
    }
}
