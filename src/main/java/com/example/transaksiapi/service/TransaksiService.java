package com.example.transaksiapi.service;

import com.example.transaksiapi.helper.CSVHelper;
import com.example.transaksiapi.helper.MapperHelper;
import com.example.transaksiapi.model.Employee;
import com.example.transaksiapi.model.entity.Fee;
import com.example.transaksiapi.model.entity.Transaksi;
import com.example.transaksiapi.repository.IFeeRepository;
import com.example.transaksiapi.repository.ITransaksiRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;

@Service
public class TransaksiService {
    @Value("${uri.employee.api}")
    private String uriEmployeeApi;
    @Autowired
    ITransaksiRepository transaksiRepository;

    @Autowired
    IFeeRepository feeRepository;

    public void save(MultipartFile file){
        try {
            List<Transaksi> listTransaksi = CSVHelper.csvToListTransaksi(file.getInputStream());
            transaksiRepository.saveAll(listTransaksi);

            //calculate and save fee
            for(Transaksi trx : listTransaksi){
                BigDecimal feeAmount = new BigDecimal(0);

                String uri = uriEmployeeApi + "?employeeId=" + trx.getEmployeeId();
                RestTemplate restTemplate = new RestTemplate();
                String result = restTemplate.getForObject(uri, String.class);
                JSONObject jsonRes = new JSONObject(result);
                JSONArray jsonArr = jsonRes.getJSONArray("data");
                if(jsonArr.length() > 0) {
                    Type type = new TypeToken<List<Employee>>() {}.getType();
                    List<Employee> listHierarchy = new Gson().fromJson(jsonArr.toString(), type);
                    for(Employee empl : listHierarchy){
                        //search by filter
                        Transaksi tempTrx = listTransaksi.stream()
                                .filter(transaksi -> empl.getEmployee_id().equals(transaksi.getEmployeeId()))
                                .findAny()
                                .orElse(null);
                        if(tempTrx != null){
                            if(empl.getPath_level() == 0){
                                //path level == 0
                                feeAmount = tempTrx.getAmount();
                            } else {
                                //path level > 0
                                feeAmount = feeAmount.add(tempTrx.getAmount().multiply(new BigDecimal(0.3)));
                            }
                        }
                    }
                    //insert to fee table
                    Fee fee = MapperHelper.toFee(trx, feeAmount);
                    feeRepository.save(fee);
                }
            }
        }catch (IOException e){
            throw new RuntimeException("Fail to store csv data: " + e.getMessage());
        }
    }
}
