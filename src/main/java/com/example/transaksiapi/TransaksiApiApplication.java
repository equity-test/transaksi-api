package com.example.transaksiapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransaksiApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransaksiApiApplication.class, args);
	}

}
